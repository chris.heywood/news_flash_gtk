use super::category::FeedListCategoryModel;
use super::feed::FeedListFeedModel;
use diffus::{Diffus, Same};
use news_flash::models::{CategoryID, FeedID};
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;

#[derive(Clone, Debug)]
pub enum FeedListItemID {
    Category(CategoryID),
    Feed(FeedID, CategoryID),
}

#[derive(Diffus, Eq, Clone, Debug, Serialize, Deserialize)]
pub enum FeedListItem {
    Feed(FeedListFeedModel),
    Category(FeedListCategoryModel),
}

impl Same for FeedListItem {
    fn same(&self, other: &Self) -> bool {
        match self {
            FeedListItem::Feed(self_feed) => match other {
                FeedListItem::Feed(other_feed) => self_feed.same(&other_feed),
                FeedListItem::Category(_other_category) => false,
            },
            FeedListItem::Category(self_category) => match other {
                FeedListItem::Feed(_other_feed) => false,
                FeedListItem::Category(other_category) => self_category.same(&other_category),
            },
        }
    }
}

impl PartialEq for FeedListItem {
    fn eq(&self, other: &FeedListItem) -> bool {
        match other {
            FeedListItem::Category(other_category) => match self {
                FeedListItem::Category(self_category) => other_category.id == self_category.id,
                FeedListItem::Feed(_) => false,
            },
            FeedListItem::Feed(other_feed) => match self {
                FeedListItem::Feed(self_feed) => other_feed.id == self_feed.id,
                FeedListItem::Category(_) => false,
            },
        }
    }
}

impl Ord for FeedListItem {
    fn cmp(&self, other: &FeedListItem) -> Ordering {
        match self {
            FeedListItem::Feed(self_feed) => match other {
                FeedListItem::Feed(other_feed) => self_feed.sort_index.cmp(&other_feed.sort_index),
                FeedListItem::Category(other_category) => self_feed.sort_index.cmp(&other_category.sort_index),
            },
            FeedListItem::Category(self_category) => match other {
                FeedListItem::Feed(other_feed) => self_category.sort_index.cmp(&other_feed.sort_index),
                FeedListItem::Category(other_category) => self_category.sort_index.cmp(&other_category.sort_index),
            },
        }
    }
}

impl PartialOrd for FeedListItem {
    fn partial_cmp(&self, other: &FeedListItem) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
