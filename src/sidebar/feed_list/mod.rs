pub mod category_row;
pub mod feed_row;
pub mod models;

use crate::app::Action;
use crate::main_window_state::MainWindowState;
use crate::settings::Settings;
use crate::sidebar::feed_list::{
    category_row::CategoryRow,
    feed_row::FeedRow,
    models::{
        EditedFeedListItem, FeedListCategoryModel, FeedListDndAction, FeedListFeedModel, FeedListItem, FeedListItemID,
        FeedListTree,
    },
};
use crate::sidebar::{SidebarIterateItem, SidebarSelection};
use crate::util::{BuilderHelper, GtkUtil, Util, NEWSFLASH_UNCATEGORIZED};
use diffus::{
    edit::enm::Edit as EnumEdit,
    edit::{collection, Edit},
    Diffable,
};
use gdk::{DragAction, EventType};
use glib::{clone, source::Continue, Sender, SourceId};
use gtk::{
    self, prelude::*, DestDefaults, Inhibit, ListBox, ListBoxRow, ScrolledWindow, SelectionMode, TargetEntry,
    TargetFlags,
};
use log::error;
use news_flash::models::{CategoryID, FeedID, PluginCapabilities, NEWSFLASH_TOPLEVEL};
use parking_lot::RwLock;
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;

#[derive(Debug)]
pub struct FeedList {
    list: ListBox,
    scroll: ScrolledWindow,
    sender: Sender<Action>,
    categories: Arc<RwLock<HashMap<CategoryID, CategoryRow>>>,
    feeds: Arc<RwLock<HashMap<FeedID, Vec<FeedRow>>>>,
    tree: Arc<RwLock<FeedListTree>>,
    delayed_selection: Arc<RwLock<Option<SourceId>>>,
    hovered_category_expand: Arc<RwLock<Option<(SourceId, CategoryID)>>>,
    state: Arc<RwLock<MainWindowState>>,
    settings: Arc<RwLock<Settings>>,
}

impl FeedList {
    pub fn new(
        sidebar_scroll: &ScrolledWindow,
        settings: &Arc<RwLock<Settings>>,
        state: &Arc<RwLock<MainWindowState>>,
        sender: Sender<Action>,
    ) -> Self {
        let builder = BuilderHelper::new("sidebar_list");
        let list_box = builder.get::<ListBox>("sidebar_list");

        // set selection mode from NONE -> SINGLE after a delay after it's been shown
        // this ensures selection mode is in SINGLE without having a selected row in the list
        list_box.connect_show(|list| {
            glib::timeout_add_local(
                Duration::from_millis(50),
                clone!(@weak list => @default-panic, move || {
                    list.set_selection_mode(SelectionMode::Single);
                    // FIXME: disconnect self
                    Continue(false)
                }),
            );
        });

        let feed_list = FeedList {
            list: list_box,
            scroll: sidebar_scroll.clone(),
            sender,
            categories: Arc::new(RwLock::new(HashMap::new())),
            feeds: Arc::new(RwLock::new(HashMap::new())),
            tree: Arc::new(RwLock::new(FeedListTree::new())),
            delayed_selection: Arc::new(RwLock::new(None)),
            hovered_category_expand: Arc::new(RwLock::new(None)),
            state: state.clone(),
            settings: settings.clone(),
        };
        feed_list.setup_dnd();
        feed_list
    }

    pub fn widget(&self) -> ListBox {
        self.list.clone()
    }

    pub fn on_window_hidden(&self) {
        self.list.set_selection_mode(SelectionMode::None);
    }

    fn calc_item_id(row: &ListBoxRow) -> Option<FeedListItemID> {
        let category_row = row.clone().downcast::<CategoryRow>().ok();
        let feed_row = row.clone().downcast::<FeedRow>().ok();

        match category_row {
            Some(category_row) => Some(FeedListItemID::Category(category_row.category_id())),
            None => match feed_row {
                Some(feed_row) => Some(FeedListItemID::Feed(feed_row.feed_id(), feed_row.parent_id())),
                None => None,
            },
        }
    }

    pub fn on_window_show(&self) {
        // Setup a filter for the sidebar so we only see relevant feeds
        self.list.set_filter_func(Some(std::boxed::Box::new(clone!(
            @weak self.settings as settings,
            @weak self.tree as tree_handle  => @default-panic, move |row|
        {
            if settings.read().get_feed_list_only_show_relevant() {
                if let Some(tree) = tree_handle.try_read() {
                    if let Some(row_id) = Self::calc_item_id(&row) {
                        if let Some(selection) = tree.calculate_selection(&row_id) {
                            if let (FeedListItemID::Feed(_feed_id, _category_id), _title, item_count) = selection {
                                if item_count == 0 {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            true
        }))));

        glib::timeout_add_local(
            Duration::from_millis(50),
            clone!(
                @weak self.list as list,
                @weak self.state as state,
                @weak self.feeds as feeds,
                @weak self.categories as categories => @default-panic, move ||
            {
                list.set_selection_mode(SelectionMode::Single);
                if let SidebarSelection::Feed(id, parent, _label) = state.read().get_sidebar_selection() {
                    if let Some(feed_rows) = feeds.read().get(&id) {
                        for feed_row in feed_rows {
                            if &feed_row.parent_id() == parent {
                                list.select_row(Some(&*feed_row));
                                break;
                            }
                        }
                    }
                }
                else if let SidebarSelection::Category(id, _label) = state.read().get_sidebar_selection() {
                    if let Some(category_row) = categories.read().get(&id) {
                        list.select_row(Some(&*category_row));
                    }
                }
                Continue(false)
            }),
        );
    }

    fn clear_hovered_expand(hovered_category_expand: &Arc<RwLock<Option<(SourceId, CategoryID)>>>) {
        if let Some((source_id, _category_id)) = hovered_category_expand.write().take() {
            GtkUtil::remove_source(Some(source_id));
        }
    }

    fn setup_dnd(&self) {
        let entry = TargetEntry::new("FeedRow", TargetFlags::SAME_APP, 0);
        self.list
            .drag_dest_set(DestDefaults::DROP | DestDefaults::MOTION, &[entry], DragAction::MOVE);
        self.list.drag_dest_add_text_targets();
        self.list.connect_drag_motion(clone!(
            @weak self.tree as tree,
            @weak self.feeds as feeds,
            @weak self.settings as settings,
            @weak self.hovered_category_expand as hovered_category_expand,
            @weak self.categories as categories => @default-panic, move |widget, _drag_context, _x, y, _time|
        {
            // maybe we should keep track of the previous highlighted rows instead of iterating over all of them
            let children = widget.children();
            for widget in children {
                if let Some(ctx) = GtkUtil::get_dnd_style_context_widget(&widget) {
                    ctx.remove_class("drag-bottom");
                    ctx.remove_class("drag-top");
                    ctx.remove_class("drag-category");
                }
            }

            if let Some(row) = widget.row_at_y(y) {
                let alloc = row.allocation();
                let item_id = Self::calc_item_id(&row);
                let is_category = GtkUtil::listboxrow_is_category(&row);
                let height_threshold = if is_category { 4 } else { 2 };

                if y <= alloc.y + (alloc.height / height_threshold) {
                    Self::clear_hovered_expand(&hovered_category_expand);
                    if let Some(ctx) = GtkUtil::get_dnd_style_context_listboxrow(&row) {
                        ctx.add_class("drag-top");
                        return false;
                    }
                }

                if is_category
                    && y >= alloc.y + (alloc.height / height_threshold)
                    && y <= alloc.y + (alloc.height * 3 / 4)
                {
                    if let Some(ctx) = GtkUtil::get_dnd_style_context_listboxrow(&row) {
                        ctx.add_class("drag-category");
                    }

                    // expand/collapse category on 1.2s hover
                    if let Some(item_id) = item_id {
                        let hover = tree.read().calculate_selection(&item_id);
                        if let Some(hovered_item) = hover {
                            if let (FeedListItemID::Category(id), _title, _item_count) = hovered_item {
                                let mut start_hover = false;
                                if let Some((saved_source, saved_id)) = hovered_category_expand.write().take() {
                                    if saved_id != id {
                                        GtkUtil::remove_source(Some(saved_source));
                                        start_hover = true;
                                    }
                                } else {
                                    start_hover = true;
                                }

                                if start_hover {
                                    *hovered_category_expand.write() = Some((
                                        glib::timeout_add_local(Duration::from_millis(1200), clone!(
                                            @strong id,
                                            @weak tree,
                                            @weak categories,
                                            @weak hovered_category_expand,
                                            @weak feeds => @default-panic, move ||
                                        {
                                            if let Some(category_row) = categories.read().get(&id) {
                                                category_row.expand_collapse_arrow();
                                                Self::expand_collapse_category(&id, &tree, &categories, &feeds);
                                            }
                                            *hovered_category_expand.write() = None;
                                            Continue(false)
                                        })), id
                                    ));
                                }
                            }
                        }
                    }

                    return false;
                }

                Self::clear_hovered_expand(&hovered_category_expand);

                // check next visible item
                if let Some(item_id) = item_id {
                    let next_item = tree.write().calculate_next_item(&item_id, settings.read().get_feed_list_only_show_relevant());
                    if let SidebarIterateItem::SelectFeedListCategory(id) = &next_item {
                        if let Some(category_row) = categories.read().get(&id) {
                            if let Some(ctx) = GtkUtil::get_dnd_style_context_listboxrow(&*category_row)
                            {
                                ctx.add_class("drag-top");
                                return false;
                            }
                        }
                    } else if let SidebarIterateItem::SelectFeedListFeed(id, parent_id) = &next_item {
                        if let Some(feed_rows) = feeds.read().get(&id) {
                            for feed_row in feed_rows {
                                if &feed_row.parent_id() == parent_id {
                                    if let Some(ctx) = GtkUtil::get_dnd_style_context_listboxrow(&*feed_row) {
                                        ctx.add_class("drag-top");
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }

                // row after doesn't exist -> insert at last pos
                if let Some(ctx) = GtkUtil::get_dnd_style_context_listboxrow(&row) {
                    ctx.add_class("drag-bottom");
                    return false;
                }
            }

            false
        }));
        self.list.connect_drag_leave(clone!(
            @weak self.hovered_category_expand as hovered_category_expand => @default-panic, move |widget, _drag_context, _time|
        {
            Self::clear_hovered_expand(&hovered_category_expand);
            let children = widget.children();
            for widget in children {
                if let Some(ctx) = GtkUtil::get_dnd_style_context_widget(&widget) {
                    ctx.remove_class("drag-bottom");
                    ctx.remove_class("drag-top");
                }
            }
        }));
        self.list.connect_drag_drop(|widget, drag_context, _x, _y, time| {
            if let Some(target_type) = drag_context.list_targets().get(0) {
                widget.drag_get_data(drag_context, target_type, time);
                return true;
            }
            false
        });
        self.list.connect_drag_data_received(clone!(
            @weak self.tree as tree,
            @weak self.hovered_category_expand as hovered_category_expand,
            @strong self.sender as sender => @default-panic, move |widget, ctx, _x, y, selection_data, _info, time| {
            Self::clear_hovered_expand(&hovered_category_expand);
            let children = widget.children();
            for widget in children {
                if let Some(ctx) = GtkUtil::get_dnd_style_context_widget(&widget) {
                    ctx.remove_class("drag-bottom");
                    ctx.remove_class("drag-top");
                    ctx.remove_class("drag-category");
                }
            }

            if let Some(row) = widget.row_at_y(y) {
                let alloc = row.allocation();
                let index = row.index();
                let is_category = GtkUtil::listboxrow_is_category(&row);
                let height_threshold = if is_category { 4 } else { 2 };

                let index = if y < alloc.y + (alloc.height / height_threshold) {
                    index
                } else if index + 1 >= 0 {
                    index + 1
                } else {
                    index
                };

                //Util::serialize_and_save(&*tree.read(), "dnd_tree.json").expect("debug info dump");
                if let Ok((parent_category, sort_index)) = tree.read().calculate_dnd(index).map_err(|_| {
                    error!("Failed to calculate Drag&Drop action");
                }) {
                    if let Some(dnd_data_string) = selection_data.text() {
                        if dnd_data_string.contains("FeedID") {
                            if parent_category == NEWSFLASH_TOPLEVEL.clone() || parent_category == NEWSFLASH_UNCATEGORIZED.clone() {
                                log::warn!("Feed drag to TOPLEVEL not allowed");
                                return;
                            }

                            let dnd_data_string = dnd_data_string.as_str().to_owned().split_off(6);
                            let dnd_data_string: Vec<&str> = dnd_data_string.split(';').collect();
                            let feed_string =
                                dnd_data_string.get(0).expect("Didn't receive feed ID with DnD data.");
                            let feed: FeedID =
                                serde_json::from_str(feed_string).expect("Failed to deserialize FeedID.");
                            let category_string = dnd_data_string
                                .get(1)
                                .expect("Didn't receive category ID with DnD data.");
                            let current_category: CategoryID =
                                serde_json::from_str(category_string).expect("Failed to deserialize FeedID.");
                            let dnd_data = FeedListDndAction::MoveFeed(
                                feed,
                                current_category,
                                parent_category,
                                sort_index,
                            );
                            Util::send(&sender, Action::DragAndDrop(dnd_data));
                            ctx.drag_drop_done(true);
                            ctx.drop_finish(true, time);
                            return;
                        }

                        if dnd_data_string.contains("CategoryID") {
                            let category: CategoryID =
                                serde_json::from_str(&dnd_data_string.as_str().to_owned().split_off(10))
                                    .expect("Failed to deserialize CategoryID.");
                            let dnd_data =
                                FeedListDndAction::MoveCategory(category, parent_category, sort_index);
                            Util::send(&sender, Action::DragAndDrop(dnd_data));
                            ctx.drag_drop_done(true);
                            ctx.drop_finish(true, time);
                            return;
                        }
                    }
                }

                ctx.drag_drop_done(false);
                ctx.drop_finish(false, time);
            }
        }));
    }

    pub fn update(&mut self, new_tree: FeedListTree, features: &Arc<RwLock<Option<PluginCapabilities>>>) {
        let mut new_tree = new_tree;
        let mut old_tree = FeedListTree::new();

        std::mem::swap(&mut old_tree, &mut *self.tree.write());

        let expanded_categories = old_tree.get_expanded_categories();
        new_tree.apply_expanded_categories(&expanded_categories);

        let diff = old_tree.top_level.diff(&new_tree.top_level);

        let mut pos = 0;
        self.process_diff(&diff, features, &mut pos, true);

        std::mem::swap(&mut new_tree, &mut *self.tree.write());
        self.list.invalidate_filter();
    }

    fn process_diff(
        &mut self,
        diff: &Edit<'_, Vec<FeedListItem>>,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
        pos: &mut i32,
        visible: bool,
    ) {
        match diff {
            Edit::Copy(_list) => {
                // no difference
            }
            Edit::Change(diff) => {
                let _ = diff
                    .into_iter()
                    .map(|edit| {
                        match edit {
                            collection::Edit::Copy(item) => {
                                // nothing changed
                                match item {
                                    FeedListItem::Feed(_feed_item) => {
                                        *pos += 1;
                                    }
                                    FeedListItem::Category(category_item) => {
                                        *pos += category_item.len() + 1;
                                    }
                                }
                            }
                            collection::Edit::Insert(item) => match item {
                                FeedListItem::Feed(feed_item) => {
                                    self.add_feed(feed_item, pos, visible, features);
                                }
                                FeedListItem::Category(category_item) => {
                                    self.add_category(category_item, pos, visible, features);
                                }
                            },
                            collection::Edit::Remove(item) => match item {
                                FeedListItem::Feed(feed_item) => {
                                    self.remove_feed(feed_item);
                                }
                                FeedListItem::Category(category_item) => {
                                    self.remove_category(category_item);
                                }
                            },
                            collection::Edit::Change(diff) => match diff {
                                EnumEdit::VariantChanged(_, _) => {}
                                EnumEdit::Copy(_item) => {}
                                EnumEdit::AssociatedChanged(enum_diff) => match enum_diff {
                                    EditedFeedListItem::Feed(changed_feed) => match changed_feed {
                                        Edit::Copy(_feed_item) => {}
                                        Edit::Change(feed_diff) => {
                                            if let Some(feed_rows) = self.feeds.read().get(&feed_diff.id) {
                                                for feed_row in feed_rows {
                                                    if let Some(new_label) = &feed_diff.label {
                                                        feed_row.update_title(&new_label);
                                                    }
                                                    if let Some(new_count) = feed_diff.item_count {
                                                        feed_row.update_item_count(new_count);
                                                    }
                                                }
                                            }
                                            *pos += 1;
                                        }
                                    },
                                    EditedFeedListItem::Category(changed_category) => match changed_category {
                                        Edit::Copy(_category_item) => {}
                                        Edit::Change(category_diff) => {
                                            let mut visible = false;
                                            if let Some(category) = self.categories.read().get(&category_diff.id) {
                                                if let Some(new_label) = &category_diff.label {
                                                    category.update_title(&new_label);
                                                }
                                                if let Some(new_count) = category_diff.item_count {
                                                    category.update_item_count(new_count);
                                                }

                                                visible = category.is_expanded();
                                            }

                                            *pos += 1;
                                            self.process_diff(&category_diff.child_diff, features, pos, visible);
                                        }
                                    },
                                },
                            },
                        }
                    })
                    .collect::<Vec<_>>();
            }
        };
    }

    fn add_category(
        &mut self,
        category: &FeedListCategoryModel,
        pos: &mut i32,
        visible: bool,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
    ) {
        let category_widget = CategoryRow::new(category, &self.state, features, visible, self.sender.clone());

        category_widget.expander_event().connect_button_press_event(clone!(
            @weak self.feeds as feeds,
            @weak self.categories as categories,
            @strong category.id as category_id,
            @weak self.tree as tree => @default-panic, move |_widget, event|
        {
            if event.button() != 1 {
                return Inhibit(false);
            }
            match event.event_type() {
                EventType::ButtonPress => (),
                _ => return Inhibit(false),
            }
            Self::expand_collapse_category(&category_id, &tree, &categories, &feeds);
            Inhibit(true)
        }));

        self.list.insert(&category_widget, *pos);
        self.categories.write().insert(category.id.clone(), category_widget);

        *pos += 1;

        for child in &category.children {
            match child {
                FeedListItem::Feed(feed_item) => {
                    self.add_feed(feed_item, pos, category.expanded, features);
                }
                FeedListItem::Category(category_item) => {
                    self.add_category(category_item, pos, category.expanded, features);
                }
            }
        }
    }

    fn remove_category(&mut self, category: &FeedListCategoryModel) {
        if let Some(category_handle) = self.categories.read().get(&category.id) {
            self.list.remove(&*category_handle);
        }
        self.categories.write().remove(&category.id);

        for child in &category.children {
            match child {
                FeedListItem::Feed(feed_item) => {
                    self.remove_feed(feed_item);
                }
                FeedListItem::Category(category_item) => {
                    self.remove_category(category_item);
                }
            }
        }
    }

    fn add_feed(
        &mut self,
        feed: &FeedListFeedModel,
        pos: &mut i32,
        visible: bool,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
    ) {
        let feed_widget = FeedRow::new(feed, &self.state, features, visible, self.sender.clone());
        self.list.insert(&feed_widget, *pos);
        if self.feeds.read().contains_key(&feed.id) {
            if let Some(feed_rows) = self.feeds.write().get_mut(&feed.id) {
                feed_rows.push(feed_widget);
            }
        } else {
            self.feeds.write().insert(feed.id.clone(), vec![feed_widget]);
        }

        *pos += 1;
    }

    fn remove_feed(&mut self, feed: &FeedListFeedModel) {
        let mut is_empty_now = false;
        if let Some(feed_rows) = self.feeds.write().get_mut(&feed.id) {
            for (i, feed_row) in feed_rows.iter().enumerate() {
                if feed_row.parent_id() == feed.parent_id {
                    self.list.remove(&*feed_row);
                    feed_rows.remove(i);
                    break;
                }
            }
            if feed_rows.is_empty() {
                is_empty_now = true;
            }
        }
        if is_empty_now {
            self.feeds.write().remove(&feed.id);
        }
    }

    pub fn expand_collapse_selected_category(&self) {
        if let Some(row) = self.list.selected_row() {
            if let Some(selected_id) = Self::calc_item_id(&row) {
                let selection = self.tree.read().calculate_selection(&selected_id);
                if let Some((FeedListItemID::Category(id), _title, _item_count)) = selection {
                    if let Some(category_row) = self.categories.read().get(&id) {
                        category_row.expand_collapse_arrow();
                        Self::expand_collapse_category(&id, &self.tree, &self.categories, &self.feeds);
                    }
                }
            }
        }
    }

    fn expand_collapse_category(
        category_id: &CategoryID,
        tree: &Arc<RwLock<FeedListTree>>,
        categories: &Arc<RwLock<HashMap<CategoryID, CategoryRow>>>,
        feeds: &Arc<RwLock<HashMap<FeedID, Vec<FeedRow>>>>,
    ) {
        if let Some((feed_ids, category_ids, expaneded)) = tree.write().collapse_expand_category(category_id) {
            for (feed_id, parent_id) in feed_ids {
                if let Some(feed_rows) = feeds.read().get(&feed_id) {
                    for feed_row in feed_rows {
                        if feed_row.parent_id() == parent_id {
                            if expaneded {
                                feed_row.expand();
                            } else {
                                feed_row.collapse();
                            }
                        }
                    }
                }
            }
            for category_id in category_ids {
                if let Some(category_row) = categories.read().get(&category_id) {
                    if expaneded {
                        category_row.expand();
                    } else {
                        category_row.collapse();
                    }
                }
            }
        }
    }

    pub fn deselect(&self) {
        self.list.unselect_all();
    }

    pub fn get_selection(&self) -> Option<(FeedListItemID, String, i64)> {
        if let Some(row) = self.list.selected_row() {
            if let Some(selected_id) = Self::calc_item_id(&row) {
                return self.tree.read().calculate_selection(&selected_id);
            }
        }
        None
    }

    pub fn get_first_item(&self) -> Option<FeedListItemID> {
        self.tree
            .write()
            .get_first(self.settings.read().get_feed_list_only_show_relevant())
    }

    pub fn get_last_item(&self) -> Option<FeedListItemID> {
        self.tree
            .write()
            .get_last(self.settings.read().get_feed_list_only_show_relevant())
    }

    pub fn set_selection(&self, selection: FeedListItemID) {
        self.cancel_selection();

        glib::idle_add_local(clone!(
            @weak self.list as list,
            @weak self.feeds as feeds,
            @weak self.categories as categories,
            @weak self.delayed_selection as delayed_selection => @default-panic, move ||
        {
            Self::list_select(&list, &selection, &feeds, &categories);

            *delayed_selection.write() = Some(
                glib::timeout_add_local(Duration::from_millis(300), clone!(
                    @strong selection,
                    @weak feeds,
                    @weak categories,
                    @weak delayed_selection=> @default-panic, move ||
                {
                    Self::row_emmit_activate(&selection, &feeds, &categories);
                    delayed_selection.write().take();
                    Continue(false)
                }))
            );

            Continue(false)
        }));
    }

    fn list_select(
        list: &ListBox,
        selection: &FeedListItemID,
        feeds: &Arc<RwLock<HashMap<FeedID, Vec<FeedRow>>>>,
        categories: &Arc<RwLock<HashMap<CategoryID, CategoryRow>>>,
    ) {
        match selection {
            FeedListItemID::Category(category) => {
                if let Some(category_row) = categories.read().get(&category) {
                    list.select_row(Some(&*category_row));
                }
            }
            FeedListItemID::Feed(feed, parent_id) => {
                if let Some(feed_rows) = feeds.read().get(&feed) {
                    for feed_row in feed_rows {
                        if &feed_row.parent_id() == parent_id {
                            list.select_row(Some(&*feed_row));
                            break;
                        }
                    }
                }
            }
        };
    }

    fn row_emmit_activate(
        selection: &FeedListItemID,
        feeds: &Arc<RwLock<HashMap<FeedID, Vec<FeedRow>>>>,
        categories: &Arc<RwLock<HashMap<CategoryID, CategoryRow>>>,
    ) {
        match selection {
            FeedListItemID::Category(category) => {
                if let Some(category_row) = categories.read().get(&category) {
                    category_row.emit_activate();
                }
            }
            FeedListItemID::Feed(feed, parent_id) => {
                if let Some(feed_rows) = feeds.read().get(&feed) {
                    for feed_row in feed_rows {
                        if &feed_row.parent_id() == parent_id {
                            feed_row.emit_activate();
                            break;
                        }
                    }
                }
            }
        };
    }

    pub fn cancel_selection(&self) {
        GtkUtil::remove_source(self.delayed_selection.write().take());
    }

    pub fn select_next_item(&self) -> SidebarIterateItem {
        if let Some(row) = self.list.selected_row() {
            if let Some(item_id) = Self::calc_item_id(&row) {
                return self
                    .tree
                    .write()
                    .calculate_next_item(&item_id, self.settings.read().get_feed_list_only_show_relevant());
            }
        }
        SidebarIterateItem::NothingSelected
    }

    pub fn select_prev_item(&self) -> SidebarIterateItem {
        if let Some(row) = self.list.selected_row() {
            if let Some(item_id) = Self::calc_item_id(&row) {
                return self
                    .tree
                    .write()
                    .calculate_prev_item(&item_id, self.settings.read().get_feed_list_only_show_relevant());
            }
        }
        SidebarIterateItem::NothingSelected
    }

    pub fn update_offline(&self) {
        for (_key, value) in self.feeds.read().iter() {
            for feed_row in value {
                if self.state.read().get_offline() {
                    feed_row.disable_dnd();
                } else {
                    feed_row.enable_dnd();
                }
            }
        }
    }
}
