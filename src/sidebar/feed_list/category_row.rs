use crate::app::Action;
use crate::main_window_state::MainWindowState;
use crate::sidebar::feed_list::models::FeedListCategoryModel;
use crate::undo_bar::UndoActionModel;
use crate::util::{GtkUtil, Util};
use gdk::{EventMask, EventType};
use gio::{traits::ActionMapExt, Menu, MenuItem, SimpleAction};
use glib::{clone, object::IsA, Sender};
use gtk::{self, Box, EventBox, Image, Inhibit, Label, Popover, PositionType, Revealer, StateFlags, Widget};
use gtk::{prelude::*, subclass::prelude::*, CompositeTemplate};
use news_flash::models::{CategoryID, CategoryType, PluginCapabilities};
use parking_lot::RwLock;
use std::str;
use std::sync::Arc;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/resources/ui/category.ui")]
    pub struct CategoryRow {
        pub id: RwLock<CategoryID>,
        pub parent_id: RwLock<CategoryID>,
        pub expanded: Arc<RwLock<bool>>,

        #[template_child]
        pub eventbox: TemplateChild<EventBox>,
        #[template_child]
        pub revealer: TemplateChild<Revealer>,
        #[template_child]
        pub category_title: TemplateChild<Label>,
        #[template_child]
        pub level_margin: TemplateChild<Box>,
        #[template_child]
        pub item_count: TemplateChild<Label>,
        #[template_child]
        pub item_count_event: TemplateChild<EventBox>,
        #[template_child]
        pub arrow_image: TemplateChild<Image>,
        #[template_child]
        pub arrow_event: TemplateChild<EventBox>,
    }

    impl Default for CategoryRow {
        fn default() -> Self {
            CategoryRow {
                id: RwLock::new(CategoryID::new("")),
                parent_id: RwLock::new(CategoryID::new("")),
                expanded: Arc::new(RwLock::new(false)),
                eventbox: TemplateChild::default(),
                revealer: TemplateChild::default(),
                category_title: TemplateChild::default(),
                level_margin: TemplateChild::default(),
                item_count: TemplateChild::default(),
                item_count_event: TemplateChild::default(),
                arrow_image: TemplateChild::default(),
                arrow_event: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CategoryRow {
        const NAME: &'static str = "CategoryRow";
        type ParentType = gtk::ListBoxRow;
        type Type = super::CategoryRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for CategoryRow {}

    impl WidgetImpl for CategoryRow {}

    impl ContainerImpl for CategoryRow {}

    impl BinImpl for CategoryRow {}

    impl ListBoxRowImpl for CategoryRow {}
}

glib::wrapper! {
    pub struct CategoryRow(ObjectSubclass<imp::CategoryRow>)
        @extends gtk::Widget, gtk::Bin, gtk::ListBoxRow;
}

impl CategoryRow {
    pub fn new(
        model: &FeedListCategoryModel,
        state: &Arc<RwLock<MainWindowState>>,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
        visible: bool,
        sender: Sender<Action>,
    ) -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        let imp = imp::CategoryRow::from_instance(&row);

        imp.level_margin.set_margin_start(model.level * 24);
        *imp.id.write() = model.id.clone();
        *imp.parent_id.write() = model.parent_id.clone();

        Self::setup_row(
            &row,
            &imp.eventbox,
            &model.id,
            model.category_type,
            state,
            features,
            model.label.clone(),
            sender,
        );

        row.update_title(&model.label);
        row.update_item_count(model.item_count);
        Self::rotate_arrow(&imp.arrow_image.get(), model.expanded);
        if !visible {
            row.collapse();
        }

        imp.arrow_event.set_events(EventMask::BUTTON_PRESS_MASK);
        imp.arrow_event.set_events(EventMask::ENTER_NOTIFY_MASK);
        imp.arrow_event.set_events(EventMask::LEAVE_NOTIFY_MASK);

        let arrow_image = imp.arrow_image.get();
        imp.arrow_event.connect_enter_notify_event(move |_widget, _| {
            arrow_image.set_opacity(1.0);
            Inhibit(false)
        });

        let arrow_image = imp.arrow_image.get();
        imp.arrow_event.connect_leave_notify_event(move |_widget, _| {
            arrow_image.set_opacity(0.8);
            Inhibit(false)
        });

        let arrow_image = imp.arrow_image.get();
        imp.arrow_event.connect_button_press_event(
            clone!(@weak imp.expanded as expanded_handle => @default-panic, move |_widget, event| {
                if event.button() != 1 {
                    return Inhibit(false);
                }
                match event.event_type() {
                    EventType::ButtonPress => (),
                    _ => return gtk::Inhibit(false),
                }
                let expanded = *expanded_handle.read();
                Self::rotate_arrow(&arrow_image, !expanded);
                *expanded_handle.write() = !expanded;
                Inhibit(false)
            }),
        );

        row
    }

    fn rotate_arrow<W: IsA<Widget>>(arrow_image: &W, expanded: bool) {
        let context = arrow_image.style_context();

        if expanded {
            context.remove_class("forward-arrow-collapsed");
            context.add_class("forward-arrow-expanded");
        } else {
            context.remove_class("forward-arrow-expanded");
            context.add_class("forward-arrow-collapsed");
        }
    }

    pub fn is_expanded(&self) -> bool {
        let imp = imp::CategoryRow::from_instance(self);
        *imp.expanded.read()
    }

    fn setup_row(
        row: &CategoryRow,
        eventbox: &EventBox,
        id: &CategoryID,
        category_type: CategoryType,
        state: &Arc<RwLock<MainWindowState>>,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
        label: String,
        sender: Sender<Action>,
    ) {
        row.set_activatable(true);
        row.style_context().remove_class("activatable");

        eventbox.set_events(EventMask::BUTTON_PRESS_MASK);

        let mut support_mutation = false;
        if let Some(features) = features.read().as_ref() {
            support_mutation = features.contains(PluginCapabilities::MODIFY_CATEGORIES);
        }

        if support_mutation && category_type != CategoryType::Generated {
            eventbox.connect_button_press_event(clone!(
                @strong id as category_id,
                @strong label,
                @weak state,
                @weak row => @default-panic, move |_eventbox, event| {
                if event.button() != 3 {
                    return Inhibit(false);
                }

                match event.event_type() {
                    EventType::ButtonRelease | EventType::DoubleButtonPress | EventType::TripleButtonPress => {
                        return Inhibit(false)
                    }
                    _ => {}
                }

                if state.read().get_offline() {
                    return Inhibit(false);
                }

                let rename_category_dialog_action = SimpleAction::new(&format!("rename-category-{}-dialog", category_id), None);
                rename_category_dialog_action.connect_activate(clone!(@weak row, @strong sender, @strong category_id => @default-panic, move |_action, _parameter| {
                    Util::send(&sender, Action::RenameCategoryDialog(category_id.clone()));
                    if let Ok(main_window) = GtkUtil::get_main_window(&row) {
                        main_window.remove_action(&format!("rename-category-{}-dialog", category_id));
                    }
                }));

                let delete_category_action = SimpleAction::new(&format!("enqueue-delete-{}-category", category_id), None);
                delete_category_action.connect_activate(clone!(
                    @weak row,
                    @strong label,
                    @strong category_id,
                    @strong sender => @default-panic, move |_action, _parameter|
                {
                    let remove_action = UndoActionModel::DeleteCategory(category_id.clone(), label.clone());
                    Util::send(&sender, Action::UndoableAction(remove_action));

                    if let Ok(main_window) = GtkUtil::get_main_window(&row) {
                        main_window.remove_action(&format!("enqueue-delete-{}-category", category_id));
                    }
                }));

                if let Ok(main_window) = GtkUtil::get_main_window(&row) {
                    main_window.add_action(&delete_category_action);
                    main_window.add_action(&rename_category_dialog_action);
                }

                let model = Menu::new();

                let rename_category_item = MenuItem::new(Some("Rename"), None);
                rename_category_item.set_action_and_target_value(Some(&format!("rename-category-{}-dialog", category_id)), None);
                model.append_item(&rename_category_item);

                let delete_category_item = MenuItem::new(Some("Delete"), None);
                delete_category_item.set_action_and_target_value(Some(&format!("enqueue-delete-{}-category", category_id)), None);
                model.append_item(&delete_category_item);

                let popover = Popover::new(Some(&row));
                popover.set_position(PositionType::Bottom);
                popover.bind_model(Some(&model), Some("win"));
                popover.show();
                popover.connect_closed(clone!(@weak row => @default-panic, move |_popover| {
                    row.unset_state_flags(StateFlags::PRELIGHT);
                }));
                row.set_state_flags(StateFlags::PRELIGHT, false);

                Inhibit(true)
            }));
        }
    }

    pub fn update_item_count(&self, count: i64) {
        let imp = imp::CategoryRow::from_instance(self);

        if count > 0 {
            imp.item_count.set_label(&count.to_string());
            imp.item_count_event.set_visible(true);
        } else {
            imp.item_count_event.set_visible(false);
        }
    }

    pub fn update_title(&self, title: &str) {
        let imp = imp::CategoryRow::from_instance(self);
        imp.category_title.set_label(title);
    }

    pub fn expander_event(&self) -> EventBox {
        let imp = imp::CategoryRow::from_instance(self);
        imp.arrow_event.get()
    }

    pub fn expand_collapse_arrow(&self) {
        let imp = imp::CategoryRow::from_instance(self);

        let expanded = !*imp.expanded.read();
        *imp.expanded.write() = expanded;
        let arrow_image = imp
            .arrow_event
            .child()
            .expect("arrow_image is not child of arrow_event");
        Self::rotate_arrow(&arrow_image, expanded);
    }

    pub fn collapse(&self) {
        let imp = imp::CategoryRow::from_instance(self);
        imp.revealer.set_reveal_child(false);
        imp.revealer.style_context().add_class("hidden");
        self.set_selectable(false);
    }

    pub fn expand(&self) {
        let imp = imp::CategoryRow::from_instance(self);
        imp.revealer.set_reveal_child(true);
        imp.revealer.style_context().remove_class("hidden");
        self.set_selectable(true);
    }

    pub fn category_id(&self) -> CategoryID {
        let imp = imp::CategoryRow::from_instance(self);
        imp.id.read().clone()
    }
}
